import {showModal} from '../../components/modal/modal';
import {createElement} from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  console.log(fighter.name);
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root`,
  });
  fighterElement.innerHTML = `
    <div class="fighter-preview___fighter">
      <h3 class="fighter-preview___fighter-name">${fighter.name}</h3>
      <img class="fighter-preview___fighter-image" src="${fighter.source}" title="${fighter.name}" alt="${fighter.name}">
      <span class="fighter-preview___fighter-attack">Attack: ${fighter.attack}</span>
      <span class="fighter-preview___fighter-defense">Defense: ${fighter.defense}</span>
      <span class="fighter-preview___fighter-health">Health: ${fighter.health}</span>
    </div>`;
    
  showModal({
    title: "Winner",
    bodyElement: fighterElement,
    // onClose: null
  });
  // call showModal function 
}