import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter){
    fighterElement.innerHTML = `
    <div class="fighter-preview___fighter">
      <h3 class="fighter-preview___fighter-name">${fighter.name}</h3>
      <img class="fighter-preview___fighter-image" src="${fighter.source}" title="${fighter.name}" alt="${fighter.name}">
      <span class="fighter-preview___fighter-attack">Attack: ${fighter.attack}</span>
      <span class="fighter-preview___fighter-defense">Defense: ${fighter.defense}</span>
      <span class="fighter-preview___fighter-health">Health: ${fighter.health}</span>
    </div>`;
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
