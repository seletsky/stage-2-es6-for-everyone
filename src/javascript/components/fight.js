import {
  controls
} from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    // status keys is pressed or is down
    const keyIsPressed = {};
    keyIsPressed[controls.PlayerOneAttack] = false;
    keyIsPressed[controls.PlayerOneBlock] = false;
    keyIsPressed[controls.PlayerTwoAttack] = false;
    keyIsPressed[controls.PlayerTwoBlock] = false;

    keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] = false;
    keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] = false;
    keyIsPressed[controls.PlayerOneCriticalHitCombination[2]] = false;

    keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] = false;
    keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] = false;
    keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]] = false;

    function keysIsAction(keyIsPressed, controlsAction, isUp, callback) {
      if(!keyIsPressed[controlsAction] && !isUp){
        keyIsPressed[controlsAction] = !keyIsPressed[controlsAction];
        callback();
      }
      if(keyIsPressed[controlsAction] && isUp){
        keyIsPressed[controlsAction] = !keyIsPressed[controlsAction];
      }
    }

    function toggleKeys(key, isUp = false) {
      switch (key) {
        case controls.PlayerOneAttack:
          keysIsAction(keyIsPressed, controls.PlayerOneAttack, isUp, ()=>{
            if(keyIsPressed[controls.PlayerOneBlock]){
              return null;
            }
            const damage = getDamage(firstFighter, secondFighter);
            const blockPower = keyIsPressed[controls.PlayerTwoBlock] ? getBlockPower(secondFighter) : 0;   
            if (blockPower){
              return null;
            }
            let currentIndicator = parseFloat(document.getElementById("right-fighter-indicator").style.width);
            let percange = secondFighter.health / currentIndicator;
            
            const hitDamage = (damage > blockPower) ? (damage - blockPower) : 0;
            secondFighter.health -= hitDamage;
            const result = ((secondFighter.health / percange) < 0) ? 0 : (secondFighter.health / percange);
            document.getElementById("right-fighter-indicator").style.width = `${result}%`;

            if(result === 0){
              resolve(firstFighter);
            }
          });
          break;
        case controls.PlayerOneBlock:
          keysIsAction(keyIsPressed, controls.PlayerOneBlock, isUp, ()=>{});
          break;

        case controls.PlayerOneCriticalHitCombination[0]:
          if(isUp){
            keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] = false;
          } else {
            keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] = true;
            if(keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] &&
               keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] &&
               keyIsPressed[controls.PlayerOneCriticalHitCombination[2]]){
                keysIsAction(keyIsPressed, controls.PlayerOneAttack, isUp, ()=>{
                  const damage = getHitPower(firstFighter);
                  let currentIndicator = parseFloat(document.getElementById("right-fighter-indicator").style.width);
                  let percange = secondFighter.health / currentIndicator;
                  
                  secondFighter.health -= damage;
                  const result = ((secondFighter.health / percange) < 0) ? 0 : (secondFighter.health / percange);
                  document.getElementById("right-fighter-indicator").style.width = `${result}%`;
      
                  if(result === 0){
                    resolve(firstFighter);
                  }
                });
            }
          }
          break;

          case controls.PlayerOneCriticalHitCombination[1]:
            if(isUp){
              keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] = false;
            } else {
              keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] = true;
              if(keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] &&
                 keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] &&
                 keyIsPressed[controls.PlayerOneCriticalHitCombination[2]]){
                  keysIsAction(keyIsPressed, controls.PlayerOneAttack, isUp, ()=>{
                    const damage = getHitPower(firstFighter);
                    let currentIndicator = parseFloat(document.getElementById("right-fighter-indicator").style.width);
                    let percange = secondFighter.health / currentIndicator;
                    
                    secondFighter.health -= damage;
                    const result = ((secondFighter.health / percange) < 0) ? 0 : (secondFighter.health / percange);
                    document.getElementById("right-fighter-indicator").style.width = `${result}%`;
        
                    if(result === 0){
                      resolve(firstFighter);
                    }
                  });
              }
            }
          break;

          case controls.PlayerOneCriticalHitCombination[2]:
            if(isUp){
              keyIsPressed[controls.PlayerOneCriticalHitCombination[2]] = false;
            } else {
              keyIsPressed[controls.PlayerOneCriticalHitCombination[2]] = true;
              if(keyIsPressed[controls.PlayerOneCriticalHitCombination[0]] &&
                 keyIsPressed[controls.PlayerOneCriticalHitCombination[1]] &&
                 keyIsPressed[controls.PlayerOneCriticalHitCombination[2]]){
                  keysIsAction(keyIsPressed, controls.PlayerOneAttack, isUp, ()=>{
                    const damage = getHitPower(firstFighter);
                    let currentIndicator = parseFloat(document.getElementById("right-fighter-indicator").style.width);
                    let percange = secondFighter.health / currentIndicator;
                    
                    secondFighter.health -= damage;
                    const result = ((secondFighter.health / percange) < 0) ? 0 : (secondFighter.health / percange);
                    document.getElementById("right-fighter-indicator").style.width = `${result}%`;
        
                    if(result === 0){
                      resolve(firstFighter);
                    }
                  });
              }
            }
          break;
        case controls.PlayerTwoAttack:
          keysIsAction(keyIsPressed, controls.PlayerTwoAttack, isUp, ()=>{
            if(keyIsPressed[controls.PlayerTwoBlock]){
              return null;
            }
            const damage = getDamage(secondFighter, firstFighter);
            const blockPower = keyIsPressed[controls.PlayerOneBlock] ? getBlockPower(firstFighter) : 0;   
            if (blockPower){
              return null;
            }
            let currentIndicator = parseFloat(document.getElementById("left-fighter-indicator").style.width);
            let percange = firstFighter.health / currentIndicator;
            
            const hitDamage = (damage > blockPower) ? (damage - blockPower) : 0;
            firstFighter.health -= hitDamage;
            const result = ((firstFighter.health / percange) < 0) ? 0 : (firstFighter.health / percange);
            document.getElementById("left-fighter-indicator").style.width = `${result}%`;

            if(result === 0){
              resolve(secondFighter);
            }
          });
          break;
        case controls.PlayerTwoBlock:
          keysIsAction(keyIsPressed, controls.PlayerTwoBlock, isUp, ()=>{});
          break;
          case controls.PlayerTwoCriticalHitCombination[0]:
            if(isUp){
              keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] = false;
            } else {
              keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] = true;
              if(keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
                 keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
                 keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]]){
                  keysIsAction(keyIsPressed, controls.PlayerTwoAttack, isUp, ()=>{
                    const damage = getHitPower(secondFighter);
                    let currentIndicator = parseFloat(document.getElementById("left-fighter-indicator").style.width);
                    let percange = firstFighter.health / currentIndicator;
                    
                    firstFighter.health -= damage;
                    const result = ((firstFighter.health / percange) < 0) ? 0 : (firstFighter.health / percange);
                    document.getElementById("left-fighter-indicator").style.width = `${result}%`;
        
                    if(result === 0){
                      resolve(secondFighter);
                    }
                  });
              }
            }
            break;
  
            case controls.PlayerTwoCriticalHitCombination[1]:
              if(isUp){
                keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] = false;
              } else {
                keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] = true;
                if(keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
                   keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
                   keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]]){
                    keysIsAction(keyIsPressed, controls.PlayerTwoAttack, isUp, ()=>{
                      const damage = getHitPower(secondFighter);
                      let currentIndicator = parseFloat(document.getElementById("left-fighter-indicator").style.width);
                      let percange = firstFighter.health / currentIndicator;
                      
                      firstFighter.health -= damage;
                      const result = ((firstFighter.health / percange) < 0) ? 0 : (firstFighter.health / percange);
                      document.getElementById("left-fighter-indicator").style.width = `${result}%`;
          
                      if(result === 0){
                        resolve(secondFighter);
                      }
                    });
                }
              }
            break;
  
            case controls.PlayerTwoCriticalHitCombination[2]:
              if(isUp){
                keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]] = false;
              } else {
                keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]] = true;
                if(keyIsPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
                   keyIsPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
                   keyIsPressed[controls.PlayerTwoCriticalHitCombination[2]]){
                    keysIsAction(keyIsPressed, controls.PlayerTwoAttack, isUp, ()=>{
                      const damage = getHitPower(secondFighter);
                      let currentIndicator = parseFloat(document.getElementById("left-fighter-indicator").style.width);
                      let percange = firstFighter.health / currentIndicator;
                      
                      firstFighter.health -= damage;
                      const result = ((firstFighter.health / percange) < 0) ? 0 : (firstFighter.health / percange);
                      document.getElementById("left-fighter-indicator").style.width = `${result}%`;
          
                      if(result === 0){
                        resolve(secondFighter);
                      }
                    });
                }
              }
            break;
      }
    }
    document.addEventListener('keydown', (keyEvent) => {
      toggleKeys(keyEvent.code);
    });

    document.addEventListener('keyup', (keyEvent) => {
      toggleKeys(keyEvent.code, true);
    });
    document.getElementById("left-fighter-indicator").style.width = '100%';
    document.getElementById("right-fighter-indicator").style.width = '100%';
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() * 2 + 1;
  const hitPower = criticalHitChance * fighter.attack;
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() * 2 + 1;
  const blockPower = dodgeChance * fighter.defense;
  return blockPower;
}